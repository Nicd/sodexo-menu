# Sodexo Hermia 5 & 6 weekly menu

View live at https://nicd.gitlab.io/sodexo-menu

* Gets menu data from Sodexo API and displays fancy table.
* 100 % strict TypeScript, no dependencies, no node modules.
* Requires modern browser, no IE support.
* Licensed under AGPL 3.0. See LICENSE file.

## To build

Install TypeScript so that you have `tsc` available. It's suggested to use the Node.js
version specified in `.tool-versions`. Then run `tsc`. Serve with your
favourite HTTP server such as `python -m http.server 53593`. Accessing from `file://`
URL will not work.
