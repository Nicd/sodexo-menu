import { RestaurantId } from './config.js';
import { ServerBlob } from './types.js';

const API_URL = new URL('https://www.sodexo.fi/ruokalistat/output/weekly_json/');

export class SodexoAPIError extends Error {
  public restaurantId: RestaurantId;

  constructor(message: string, restaurantId: RestaurantId) {
    super(message);
    this.restaurantId = restaurantId;
  }
}

export async function getMenu(restaurantId: RestaurantId): Promise<ServerBlob> {
  const url = new URL(String(restaurantId), API_URL);

  try {
    const resp = await fetch(url.toString());
    if (resp.ok) {
      return await resp.json();
    } else {
      console.error(resp);
      throw new SodexoAPIError(
        `Got invalid response: ${resp.status} ${resp.statusText}.`,
        restaurantId
      );
    }
  } catch (e) {
    console.error(e);
    throw new SodexoAPIError(`Request error.`, restaurantId);
  }
}

export type MenuResult = ServerBlob | SodexoAPIError;

export async function getAllMenus(restaurants: readonly RestaurantId[]): Promise<MenuResult[]> {
  const promises: Promise<MenuResult>[] = [];

  for (const restaurant of restaurants) {
    promises.push(
      new Promise(async resolve => {
        try {
          resolve(await getMenu(restaurant));
        } catch (e) {
          resolve(e);
        }
      })
    );
  }

  return await Promise.all(promises);
}
