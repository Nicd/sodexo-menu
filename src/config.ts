/** Restaurant IDs to fetch data for. */
export enum RestaurantId {
  HERMIA5 = 107,
  HERMIA6 = 110,
};

/** Define order of restaurants. */
export const RESTAURANTS: readonly RestaurantId[] = [
  RestaurantId.HERMIA5,
  RestaurantId.HERMIA6,
] as const;

/** ID of HTML tag to use in DOM for root element. */
export const HTML_ID = 'menu_el';

/** Names of days in the API. */
export const DAY_NAMES = [
  'Maanantai',
  'Tiistai',
  'Keskivikko',
  'Torstai',
  'Perjantai'
] as const;
