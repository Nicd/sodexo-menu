interface DOMOptions {
  id?: string;
  classes?: string[];
  text?: string;
}

interface DOMAttributes {
  [key: string]: string;
}

export function el<K extends keyof HTMLElementTagNameMap>(type: K, opts?: DOMOptions, attrs?: DOMAttributes): HTMLElementTagNameMap[K];
export function el(type: string, opts?: DOMOptions, attrs?: DOMAttributes): HTMLElement {
  const elem = document.createElement(type);

  if (opts !== undefined) {
    if (opts.id) {
      elem.id = opts.id;
    }

    if (opts.classes) {
      for (const cls of opts.classes) { elem.classList.add(cls) }
    }

    if (opts.text) {
      elem.textContent = opts.text;
    }
  }

  if (attrs !== undefined) {
    for (const [name, val] of Object.entries(attrs)) {
      elem.setAttribute(name, val);
    }
  }

  return elem;
}
