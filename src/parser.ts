import { ServerBlob, MenuData, MenuMeta, RestaurantInfo, DayData, Course, ServerMealdate } from './types.js';
import { RestaurantId } from './config.js';

export class ParseError extends Error { }

function parseMealdate(mealdate: ServerMealdate): DayData {
  const coursekeys = Object.keys(mealdate.courses).sort();
  const courses: Course[] = [];
  for (const coursekey of coursekeys) {
    const serverCourse = mealdate.courses[coursekey];
    courses.push({
      titleFi: serverCourse.title_fi,
      titleEn: serverCourse.title_en,
      category: serverCourse.category,
      properties: serverCourse.properties,
      price: serverCourse.price
    });
  }

  return {
    date: mealdate.date,
    courses: courses
  };
}

export function parseBlob(blob: ServerBlob, id: RestaurantId): MenuData {
  try {
    const meta: MenuMeta = {
      generated: new Date(blob.meta.generated_timestamp * 1000),
      timeperiod: blob.timeperiod
    };

    const restaurantInfo: RestaurantInfo = {
      name: blob.meta.ref_title,
      url: blob.meta.ref_url
    };

    const days: DayData[] = [];
    for (const day of blob.mealdates) {
      days.push(parseMealdate(day));
    }

    return {
      id,
      meta,
      restaurantInfo,
      days
    };
  } catch (e) {
    console.error(e);
    throw new ParseError(`Got error while parsing blob: ${e}\n\nBlob:\n${blob}`)
  }
}
