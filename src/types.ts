import { RestaurantId } from './config';

export interface ServerMeta {
  readonly generated_timestamp: number;
  readonly ref_url: string;
  readonly ref_title: string;
}

export interface ServerCourse {
  readonly title_fi: string;
  readonly title_en: string;
  readonly category: string;
  readonly properties: string;
  readonly price: string;
}

export interface ServerMealdate {
  readonly date: string;
  readonly courses: { [key: string]: ServerCourse };
}

export interface ServerBlob {
  readonly meta: ServerMeta;
  readonly timeperiod: string;
  readonly mealdates: ServerMealdate[];
}

export interface RestaurantInfo {
  readonly url: string;
  readonly name: string;
}

export interface MenuMeta {
  readonly generated: Date;
  readonly timeperiod: string;
}

export interface Course {
  readonly titleFi: string;
  readonly titleEn: string;
  readonly category: string;
  readonly properties: string;
  readonly price: string;
}

export interface DayData {
  readonly date: string;
  readonly courses: Course[];
}

export interface MenuData {
  readonly id: RestaurantId;
  readonly restaurantInfo: RestaurantInfo;
  readonly meta: MenuMeta;
  readonly days: DayData[];
}
