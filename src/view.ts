import { MenuData, Course } from './types.js';
import { el } from './dom.js';
import { SodexoAPIError } from './sodexo-api.js';
import { RestaurantId } from './config.js';

function renderCourse(course: Course): HTMLLIElement {
  const li = el('li');
  const nameFi = el('p', { classes: ['course-name-fi'], text: course.titleFi }, { lang: 'fi' });
  const nameEn = el('p', { classes: ['course-name-en'], text: course.titleEn }, { lang: 'en' });
  const category = el('p', { classes: ['course-category'], text: course.category });
  const properties = el('p', { classes: ['course-properties'], text: course.properties });
  const price = el('p', { classes: ['course-price'], text: course.price });

  li.appendChild(nameFi);
  li.appendChild(nameEn);
  li.appendChild(category);
  li.appendChild(properties);
  li.appendChild(price);
  return li;
}

export function renderMenu(menu: MenuData): HTMLElement[] {
  const heading = el('h2');
  const headingLink = el(
    'a',
    { text: `${menu.restaurantInfo.name}, ${menu.meta.timeperiod}` },
    { href: menu.restaurantInfo.url, target: '_blank' }
  );
  heading.appendChild(headingLink);

  const dayElements = [];
  for (const day of menu.days) {
    const dayEl = el('div', { classes: ['day'] }, { 'data-day-name': day.date });
    const coursesList = el('ul', {}, { 'aria-label': day.date });
    for (const course of day.courses) {
      coursesList.appendChild(renderCourse(course));
    }
    dayEl.appendChild(coursesList);
    dayElements.push(dayEl);
  }

  const metaElem = el('p', {
    classes: ['meta'],
    text: `Päivitetty ${menu.meta.generated.toISOString()}.`
  });

  return [heading, ...dayElements, metaElem];
}

export function renderError(data: SodexoAPIError): HTMLElement {
  const errorDiv = el('div', { classes: ['error'] });
  errorDiv.appendChild(
    el('h2', { text: `Unable to fetch menu for "${RestaurantId[data.restaurantId]}":` })
  );
  errorDiv.appendChild(el('p', { text: data.message }));
  errorDiv.appendChild(el('p', { classes: ['stack'], text: data.stack }));
  return errorDiv;
}
