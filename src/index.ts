import { HTML_ID, RESTAURANTS, DAY_NAMES } from './config.js';
import { getAllMenus, SodexoAPIError } from './sodexo-api.js';
import { parseBlob } from './parser.js';
import { renderMenu, renderError } from './view.js';
import { el } from './dom.js';
import { VERSION, SOURCE } from './version.js';

async function init() {
  const rootEl = document.getElementById(HTML_ID);
  if (rootEl === null) {
    throw new Error('Cannot init DOM element.');
  }

  rootEl.innerText = 'Loading…';

  const menus = await getAllMenus(RESTAURANTS);
  const parsedMenus = menus.map((m, idx) => {
    if (!(m instanceof SodexoAPIError)) {
      return parseBlob(m, RESTAURANTS[idx]);
    } else {
      return m;
    }
  });

  rootEl.innerText = '';

  const dayElements: HTMLElement[] = [];
  for (const day of DAY_NAMES) {
    dayElements.push(
      el('div', { classes: ['day-heading'], text: day })
    );
  }

  for (const data of parsedMenus) {
    if (data instanceof SodexoAPIError) {
      rootEl.appendChild(renderError(data));
    } else {
      const [heading, ...elems] = renderMenu(data);

      rootEl.appendChild(heading);
      for (const day of DAY_NAMES) {
        rootEl.appendChild(
          el(
            'div',
            {
              classes: ['day-heading'],
              text: day
            },
            {
              'aria-hidden': 'true'
            }
          )
        );
      }
      for (const elem of elems) { rootEl.appendChild(elem); }
    }
  }

  const versionEl = el('div', { classes: ['version'], text: `Versio ${VERSION}. ` });
  versionEl.appendChild(
    el('a', { text: 'GitLab' }, { href: SOURCE, target: '_blank' })
  );
  versionEl.appendChild(document.createTextNode('.'));
  rootEl.appendChild(versionEl);
}

if (document.readyState !== 'loading') {
  init();
} else {
  document.addEventListener('DOMContentLoaded', init);
}
